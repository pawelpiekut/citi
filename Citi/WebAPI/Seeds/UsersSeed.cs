﻿using CoinShop.API.Dtos;
using CoinShop.API.Services;
using CoinShop.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Seeds
{
    public class UsersSeed
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IConfiguration _configuration;
        private readonly IAuthService _authService;
        private readonly IUserRolesService _userRolesService;

        public UsersSeed(
            IUnitOfWork unitOfWork,
            IAuthService authService,
            IConfiguration configuration,
            IUserRolesService userRolesService)
        {
            _unitOfWork = unitOfWork;
            _authService = authService;
            _configuration = configuration;
            _userRolesService = userRolesService;
        }

        public void Seed()
        {
            SeedRoles();
            CreateUserWithRole("Pawel", "Piekut", "pawelp", "pawel@piekut.org", "@dmin1234", new List<string>() { "COIN_ADD", "COIN_DELETE", "COIN_UPDATE", "COIN_READ" });
            CreateUserWithRole("Reader", "Reader", "reader", "reader@reader.org", "@dmin1234", new List<string>() { "COIN_READ" });
            CreateUserWithRole("Noone", "Noone", "noone", "noone@noone.org", "@dmin1234", new List<string>() { });
        }

        private void SeedRoles()
        {
            if(_unitOfWork.Roles.GetAll().Count() == 0)
            {
                _unitOfWork.Roles.Add(new Domain.Entities.Role() { RoleName = "COIN_ADD" });
                _unitOfWork.Roles.Add(new Domain.Entities.Role() { RoleName = "COIN_DELETE" });
                _unitOfWork.Roles.Add(new Domain.Entities.Role() { RoleName = "COIN_UPDATE" });
                _unitOfWork.Roles.Add(new Domain.Entities.Role() { RoleName = "COIN_READ" });

                _unitOfWork.Commit();
            }
        }

        private void CreateUserWithRole(string firstname, string lastname, string username, string email, string password, IList<string> roles)
        {
            if(!CheckIfUsernameExists(username))
            {
                var createdUser = _authService.Register(new AddUserRequest()
                {
                    Firstname = firstname,
                    Lastname = lastname,
                    Email = email,
                    Username = username,
                    Password = password
                })
                    .GetAwaiter()
                    .GetResult();

                var response = _userRolesService.ModifyRolesToUser(new ModifyUserRolesRequest()
                {
                    Username = username,
                    Roles = roles.ToList()
                })
                    .GetAwaiter()
                    .GetResult();
            }
        }

        private bool CheckIfUsernameExists(string username)
        {
            return _unitOfWork.Users.GetAll().Where(u => username.Equals(u.Username)).Any();
        }
    }
}
