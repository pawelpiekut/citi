﻿using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Seeds
{
    public class CoinsSeed
    {
        private readonly IUnitOfWork _unitOfWork;
        public CoinsSeed(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Seed() 
        {
            if(_unitOfWork.Coins.GetAll().Count() == 0){
                _unitOfWork.Coins.Add(new Coin()
                {
                    LongName = "InitCoin",
                    ShortName = "InitCoin",
                    Price = 13,
                    Weight = 9
                })
                    .GetAwaiter()
                    .GetResult();
                _unitOfWork.Commit();
            }
        }
    }
}
