﻿using CoinShop.API.Dtos.Coin;
using CoinShop.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CoinsController : ControllerBase
    {
        private readonly ICoinService _coinService;

        public CoinsController(ICoinService coinService)
        {
            _coinService = coinService;
        }

        [Authorize(Roles = "COIN_ADD")]
        [HttpPost]
        [Route("Add")]
        public async Task<ActionResult> AddCoin([FromBody] AddCoinRequest addCoinRequest)
        {
            var createdCoin = await _coinService.Add(addCoinRequest);
            return Ok(createdCoin.Id);
        }

        [Authorize(Roles = "COIN_READ")]
        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult> GetallCoins()
        {
            var coinList = await _coinService.GetAll();
            return Ok(coinList);
        }

        [Authorize(Roles = "COIN_DELETE")]
        [HttpGet]
        [Route("Delete/{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            await _coinService.Delete(id);
            return Ok();
        }

        [Authorize(Roles = "COIN_UPDATE")]
        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult> Update([FromBody] UpdateCoinRequest updateCoinRequest)
        {
            await _coinService.Update(updateCoinRequest);
            return Ok();
        }
    }
}
