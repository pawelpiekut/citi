﻿using CoinShop.API.Dtos;
using CoinShop.API.Services;
using CoinShop.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly ITokenService _tokenService;
        private readonly IApplicationSettingsService _applicationSettingsService;

        public AuthController(
            IAuthService authService,
            ITokenService tokenService,
            IApplicationSettingsService applicationSettingsService
            )
        {
            _authService = authService;
            _tokenService = tokenService;
            _applicationSettingsService = applicationSettingsService;
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] AddUserRequest userForRegisterDto)
        {
            userForRegisterDto.Username = userForRegisterDto.Username.ToLower();

            if (await _authService.UserExists(userForRegisterDto.Username))
                return BadRequest("Username already exists");

            var createdUser = await _authService.Register(userForRegisterDto);

            return StatusCode(201);
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            var loginresponse = await _authService.Login(loginRequest);

            if (loginresponse == null)
                return Unauthorized();

            var tokenString = await _tokenService.GenerateAuthToken(loginresponse);

            return Ok(new
            {
                token = tokenString
            });
        }

        [Authorize]
        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequest userForChangePassword)
        {

            var user = await _authService.ChangePassword(userForChangePassword);

            if (user == null)
                return BadRequest("Wrong password");

            return StatusCode(201);
        }
    }
}
