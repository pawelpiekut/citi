﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos.Coin
{
    public class CoinDto
    {
        public Guid Id { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public decimal Price { get; set; }
        public double Weight { get; set; }
    }
}
