﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos.Coin
{
    public class GetAllCoinResponse
    {
        public IList<CoinDto> Coins { get; set; }

        public GetAllCoinResponse()
        {
            Coins = new List<CoinDto>();
        }
    }
}
