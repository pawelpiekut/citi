﻿using CoinShop.API.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos.Coin
{
    public class AddCoinRequest
    {
        [Required]
        public string ShortName { get; set; }

        [Required]
        public string LongName { get; set; }

        [Required]
        [RequiredGreaterThanZero]
        public decimal? Price { get; set; }

        [Required]
        [RequiredGreaterThanZero]
        public double? Weight { get; set; }
    }
}
