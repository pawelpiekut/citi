﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos
{
    public class ModifyUserRolesResponse
    {
        public string Username { get; set; }
        public List<string> Roles { get; set; }
    }
}
