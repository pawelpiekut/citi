﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos
{
    public class LoginResponse
    {
        public Guid Id { get; set; }
        public string Username { get; set; }

        // Other personal data
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        public IList<RoleDto> Roles { get; set; }

        public LoginResponse()
        {
            Roles = new List<RoleDto>();
        }
    }
}
