﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoinShop.API.Dtos
{
    public interface IAuthService
    {
        Task<GetUserWithRolesResponse> Register(AddUserRequest user);
        Task<LoginResponse> Login(LoginRequest loginRequest);
        Task<bool> UserExists(string username);
        Task<bool> ChangePassword(ChangePasswordRequest changePasswordRequest);
    }
}
