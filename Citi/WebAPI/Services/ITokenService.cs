﻿using CoinShop.API.Dtos;
using CoinShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public interface ITokenService
    {
        /// <summary>
        /// Generates JWT token based on user's data
        /// </summary>
        /// <param name="userFromRepo"></param>
        /// <returns></returns>
        Task<string> GenerateAuthToken(LoginResponse userFromRepo);
    }
}
