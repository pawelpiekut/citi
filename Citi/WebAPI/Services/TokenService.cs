﻿using CoinShop.API.Dtos;
using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using CoinShop.Domain.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public class TokenService : ITokenService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IApplicationSettingsService _applicationSettingsService;

        public TokenService(IUnitOfWork unitOfWork,
            IApplicationSettingsService applicationSettingsService)
        {
            _unitOfWork = unitOfWork;
            _applicationSettingsService = applicationSettingsService;
        }

        public async Task<string> GenerateAuthToken(LoginResponse userFromRepo)
        {
            var roleNames = await _unitOfWork.UserRole.GetAll()
                .Where(u => u.UserId == userFromRepo.Id)
                .ToListAsync()
                .ConfigureAwait(false);

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, userFromRepo.Username));
            roleNames.ForEach(r =>
            {
                claims.Add(new Claim(ClaimTypes.Role, r.Role.RoleName));
            });

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_applicationSettingsService.GetAppSettings().Token));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(Constants.AUTH_TOKEN_EXPIRATION_IN_DAYS),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
