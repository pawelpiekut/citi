﻿using CoinShop.API.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public interface IUserRolesService
    {
        Task<ModifyUserRolesResponse> ModifyRolesToUser(ModifyUserRolesRequest dto);
        Task<List<string>> GetRolesByUserId(Guid userId);
        Task<IList<RoleDto>> GetAllRoles();
    }
}
