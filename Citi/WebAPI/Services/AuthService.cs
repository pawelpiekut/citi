﻿using CoinShop.API.Dtos;
using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AuthService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            var user = await _unitOfWork.Users.GetAll()
                .Where(x => x.Username == changePasswordRequest.Username)
                .Where(x => x.IsDeleted == false)
                .SingleOrDefaultAsync();

            if (user == null)
                return false;

            if (!VerifyPasswordHash(changePasswordRequest.CurrentPassword, user.PasswordHash, user.PasswordSalt))
                return false;

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(changePasswordRequest.NewPassword, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _unitOfWork.Users.Update(user);
            _unitOfWork.Commit();

            return true;
        }

        public async Task<LoginResponse> Login(LoginRequest loginRequest)
        {
            User user = await _unitOfWork.Users.GetAll()
                .Where(x => x.Username == loginRequest.Username)
                .Where(x => x.IsDeleted == false)
                .Include(x => x.UserRole)
                    .ThenInclude(ur => ur.Role)
                .FirstOrDefaultAsync();

            if (user == null)
                return null;

            if (!VerifyPasswordHash(loginRequest.Password, user.PasswordHash, user.PasswordSalt))
                return null;

            return new LoginResponse()
            {
                Id = user.Id,
                Email = user.Email,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Username = user.Username,
                Roles = user.UserRole.Select(r => new RoleDto() { Name = r.Role.RoleName }).ToList()
            };
        }

        public async Task<GetUserWithRolesResponse> Register(AddUserRequest addUserRequest)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(addUserRequest.Password, out passwordHash, out passwordSalt);

            User user = new User()
            {
                Username = addUserRequest.Username,
                Email = addUserRequest.Email
            };
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.CreationTime = DateTimeOffset.UtcNow;
            user.DeletionTime = (DateTimeOffset?)null;

            await _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();

            return new GetUserWithRolesResponse()
            {
                Id = user.Id,
                Username = user.Username,
                Email = user.Email,
                Firstname = user.Firstname,
                Lastname = user.Lastname
            };
        }

        public async Task<bool> UserExists(string username)
        {
            if (await _unitOfWork.Users.Any(x => x.Username == username))
                return true;

            return false;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
            }
            return true;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
