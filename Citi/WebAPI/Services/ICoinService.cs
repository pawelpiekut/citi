﻿using CoinShop.API.Dtos.Coin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public interface ICoinService
    {
        Task<AddCoinResponse> Add(AddCoinRequest addCoinRequest);
        Task<GetAllCoinResponse> GetAll();
        Task<bool> Delete(Guid id);
        Task<UpdateCoinresponse> Update(UpdateCoinRequest updateCoinRequest);
    }
}
