﻿using CoinShop.API.Dtos.Coin;
using CoinShop.API.Mappers;
using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using CoinShop.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public class CoinService : ICoinService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CoinService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<AddCoinResponse> Add(AddCoinRequest addCoinRequest)
        {
            var coinTBeAdded = new Coin()
            {
                LongName = addCoinRequest.LongName,
                ShortName = addCoinRequest.ShortName,
                Price = addCoinRequest.Price.Value,
                Weight = addCoinRequest.Weight.Value
            };
            await _unitOfWork.Coins.Add(coinTBeAdded);
            _unitOfWork.Commit();

            return new AddCoinResponse()
            {
                LongName = coinTBeAdded.LongName,
                ShortName = coinTBeAdded.ShortName,
                Price = coinTBeAdded.Price,
                Weight = coinTBeAdded.Weight
            };
        }

        public async Task<bool> Delete(Guid id)
        {
            var coin = await _unitOfWork.Coins.GetAll().Where(c => c.Id == id).SingleOrDefaultAsync();
            if(coin == null)
            {
                throw new CoinNotFoundException("Coin not found");
            }

            _unitOfWork.Coins.Delete(coin);
            _unitOfWork.Commit();
            return true;
        }

        public async Task<GetAllCoinResponse> GetAll()
        {
            var coinList = await _unitOfWork.Coins.GetAll().ToListAsync();
            
            CoinMapper coinMapper = new CoinMapper();
            
            List<CoinDto> coinDtos = new List<CoinDto>();
            foreach(var coin in coinList)
            {
                coinDtos.Add(coinMapper.Convert(coin));
            }

            return new GetAllCoinResponse()
            {
                Coins = coinDtos
            };
        }

        public Task<UpdateCoinresponse> Update(UpdateCoinRequest updateCoinRequest)
        {
            throw new NotImplementedException();
        }
    }
}
