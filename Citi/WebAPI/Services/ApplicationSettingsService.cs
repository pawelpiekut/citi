﻿using CoinShop.Domain.Entities.App;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public class ApplicationSettingsService : IApplicationSettingsService
    {
        private readonly IConfiguration _configuration;

        public ApplicationSettingsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public AppSettings GetAppSettings()
        {
            var appSettingsSections = _configuration.GetSection("AppSettings");
            var appSettings = appSettingsSections.Get<AppSettings>();

            return appSettings;
        }
    }
}
