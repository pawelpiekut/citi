﻿using CoinShop.Domain.Entities.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public interface IApplicationSettingsService
    {
        AppSettings GetAppSettings();
    }
}
