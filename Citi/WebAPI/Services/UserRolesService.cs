﻿using CoinShop.API.Dtos;
using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Services
{
    public class UserRolesService : IUserRolesService
    {
        private IUnitOfWork _unitOfWork;

        public UserRolesService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IList<RoleDto>> GetAllRoles()
        {
            List<RoleDto> roleDtos = new List<RoleDto>();
            var roles = await _unitOfWork.Roles.GetAll().ToListAsync();
            roles.ForEach(r =>
            {
                roleDtos.Add(new RoleDto()
                {
                    Id = r.RoleId,
                    Name = r.RoleName
                });
            });
            return roleDtos;
        }

        public async Task<List<string>> GetRolesByUserId(Guid userId)
        {
            var roleNames = await _unitOfWork.UserRole.GetAll()
                .Include(ur => ur.Role)
                .Include(ur => ur.User)
                .Where(ur => ur.User.Id == userId)
                .Select(ur => ur.Role.RoleName)
                .ToListAsync()
                .ConfigureAwait(false);

            return roleNames;
        }

        public async Task<ModifyUserRolesResponse> ModifyRolesToUser(ModifyUserRolesRequest dto)
        {
            ValidateInput(dto);

            User user = await GetUser(dto.Username);
            List<Role> selectedRoles = await GetSelectedRolesByNames(dto.Roles);

            AddNewRolesToUser(user, selectedRoles);

            _unitOfWork.Commit();

            return new ModifyUserRolesResponse()
            {
                Username = user.Username,
                Roles = user.UserRole.Select(u => u.Role.RoleName).ToList()
            };
        }

        private void ValidateInput(ModifyUserRolesRequest dto)
        {
            if (string.IsNullOrEmpty(dto.Username))
            {
                throw new ArgumentNullException($"No username provided.");
            }

            if (dto.Roles == null)
            {
                throw new ArgumentNullException($"No role names provided.");
            }
        }

        private void AssignRolesToUser(User user, List<Role> roles)
        {
            foreach (var role in roles)
            {
                AssignSingleRoleToUser(user, role);
            }
        }

        private void AssignSingleRoleToUser(User user, Role role)
        {
            var userRole = user.UserRole.Where(ur => ur.RoleId == role.RoleId).SingleOrDefault();
            if (userRole == null)
            {
                user.UserRole.Add(new UserRole()
                {
                    UserId = user.Id,
                    RoleId = role.RoleId
                });
            }
        }

        private async Task<User> GetUser(string username)
        {
            var user = await _unitOfWork.Users.GetAll()
                .Where(u => u.Username.ToUpper() == username.ToUpper())
                .Include(u => u.UserRole)
                .SingleOrDefaultAsync();

            if (user == null)
            {
                throw new ArgumentException($"No user with username: '{username}'.");
            }

            return user;
        }

        private void AddNewRolesToUser(User user, List<Role> selectedRoles)
        {
            List<Role> roles = new List<Role>();
            foreach (var selectedRole in selectedRoles)
            {
                var userRoleAlreadyAdded = user.UserRole
                    .Where(ur => ur.RoleId == selectedRole.RoleId)
                    .SingleOrDefault();
                if (userRoleAlreadyAdded == null)
                {
                    roles.Add(selectedRole);
                }
            }

            AssignRolesToUser(user, roles);
        }

        private async Task<List<Role>> GetSelectedRolesByNames(List<string> selectedRoleNames)
        {
            return await _unitOfWork.Roles.GetAll()
                .Where(r => selectedRoleNames.Contains(r.RoleName))
                .ToListAsync();
        }
    }
}
