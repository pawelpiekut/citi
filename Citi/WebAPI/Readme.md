﻿1. Database creation
// dotnet ef migrations add Init --project CoinShop.Infrastructure --context ShopContext --verbose
dotnet ef database update --project CoinShop.Infrastructure --context ShopContext --verbose

2. Swagger
https://localhost:{YOUR_PORT}/swagger/index.html

3. E2E test run
! E2E tests create new database for their own purpose, so make sure that connectionString is valid for your DB !

Run it:
CoinShop.Tests.CoinGetAllTests