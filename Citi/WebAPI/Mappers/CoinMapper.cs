﻿using CoinShop.API.Dtos.Coin;
using CoinShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Mappers
{
    public class CoinMapper
    {
        public CoinDto Convert(Coin coin)
        {
            return new CoinDto()
            {
                Id = coin.Id,
                LongName = coin.LongName,
                Weight = coin.Weight,
                ShortName = coin.ShortName,
                Price = coin.Price
            };
        }
    }
}
