﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.API.Validators
{
    public class RequiredGreaterThanZero : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            double i;
            return value != null && double.TryParse(value.ToString(), out i) && i > 0;
        }
    }
}
