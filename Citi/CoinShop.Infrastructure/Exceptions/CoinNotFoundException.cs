﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Exceptions
{
    public class CoinNotFoundException: Exception
    {
        public CoinNotFoundException(string err): base(err)
        {

        }
    }
}
