﻿using CoinShop.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        IRoleRepository Roles { get; }
        IUserRoleRepository UserRole { get; }
        ICoinRepository Coins { get; }

        ShopContext Context { get; }
        void Commit();
    }
}
