﻿using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data.Repositories
{

    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(ShopContext dbContext) : base(dbContext)
        {
        }
    }
}
