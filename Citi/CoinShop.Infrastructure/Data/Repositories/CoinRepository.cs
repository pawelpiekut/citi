﻿using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using CoinShop.Infrastructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinShop.Infrastructure.Data.Repositories
{
    public class CoinRepository : BaseRepository<Coin>, ICoinRepository
    {
        public CoinRepository(ShopContext dbContext) : base(dbContext)
        {
        }
    }
}
