﻿using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data.Repositories
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(ShopContext dbContext) : base(dbContext)
        {
        }
    }
}
