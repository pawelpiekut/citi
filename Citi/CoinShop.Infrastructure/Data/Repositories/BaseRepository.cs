﻿using CoinShop.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CoinShop.Infrastructure.Data.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private ShopContext _dbContext;

        public BaseRepository(ShopContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task Add(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
        }

        public async Task AddMany(IEnumerable<TEntity> entities)
        {
            await _dbContext.Set<TEntity>().AddRangeAsync(entities);
        }

        public void Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
        }

        public void Delete(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false)
        {
            IEnumerable<TEntity> objects = GetQuery(ignoreQueryFilters).Where<TEntity>(where).AsEnumerable();
            foreach (TEntity obj in objects)
                _dbContext.Set<TEntity>().Remove(obj);
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false)
        {
            return await GetQuery(ignoreQueryFilters).Where<TEntity>(where).AnyAsync();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false)
        {
            return await GetQuery(ignoreQueryFilters).FirstOrDefaultAsync<TEntity>();
        }

        public IQueryable<TEntity> GetAll(bool ignoreQueryFilters = false)
        {
            return GetQuery(ignoreQueryFilters).AsQueryable();
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false)
        {
            return GetQuery(ignoreQueryFilters).Where(where).AsEnumerable();
        }

        private IQueryable<TEntity> GetQuery(bool ignoreQueryFilters = false)
        {
            return ignoreQueryFilters == true ? _dbContext.Set<TEntity>().IgnoreQueryFilters() :
                _dbContext.Set<TEntity>();
        }
    }
}
