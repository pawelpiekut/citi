﻿using CoinShop.Domain.Entities;
using CoinShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ShopContext dbContext) : base(dbContext)
        {
        }
    }
}
