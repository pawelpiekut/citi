﻿using CoinShop.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data.EntityConfigurations
{
    public class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> modelBuilder)
        {
            modelBuilder
            .ToTable("UserRole");

            modelBuilder
                .HasKey(ur => new { ur.UserId, ur.RoleId });
            modelBuilder
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRole)
                .HasForeignKey(ur => ur.RoleId);
            modelBuilder
                .HasOne(ur => ur.User)
                .WithMany(r => r.UserRole)
                .HasForeignKey(ur => ur.UserId);
        }
    }
}
