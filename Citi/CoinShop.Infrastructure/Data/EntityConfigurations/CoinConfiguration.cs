﻿using CoinShop.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data.EntityConfigurations
{
    class CoinConfiguration : IEntityTypeConfiguration<Coin>
    {
        public void Configure(EntityTypeBuilder<Coin> modelBuilder)
        {
            modelBuilder
                .ToTable("Coins");

            modelBuilder.Property(s => s.Id)
                .HasDefaultValueSql("NEWID()");
        }
    }
}
