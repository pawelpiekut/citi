﻿using CoinShop.Domain.Interfaces;
using CoinShop.Infrastructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IUserRepository _users;
        public IUserRepository Users
        {
            get
            {
                if (_users == null)
                {
                    _users = new UserRepository(Context);
                }
                return _users;
            }
        }

        private IRoleRepository _roles;
        public IRoleRepository Roles
        {
            get
            {
                if (_roles == null)
                {
                    _roles = new RoleRepository(Context);
                }
                return _roles;
            }
        }

        private IUserRoleRepository _userRole;
        public IUserRoleRepository UserRole
        {
            get
            {
                if (_userRole == null)
                {
                    _userRole = new UserRoleRepository(Context);
                }
                return _userRole;
            }
        }

        private ICoinRepository _coins;
        public ICoinRepository Coins
        {
            get
            {
                if (_coins == null)
                {
                    _coins = new CoinRepository(Context);
                }
                return _coins;
            }
        }

        public ShopContext Context { get; }

        public UnitOfWork(ShopContext context)
        {
            Context = context;
        }

        public void Commit()
        {
            Context.SaveChanges();
        }
    }
}
