﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Entities
{
    public class Coin
    {
        public Guid Id { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public decimal Price { get; set; }
        public double Weight { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
