﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset? DeletionTime { get; set; }
        public bool IsDeleted { get; set; } = false;
        public ICollection<UserRole> UserRole { get; set; }
    }
}
