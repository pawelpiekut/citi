﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Entities.App
{
    public class AppSettings
    {
        public string Token { get; set; }
        public string Salt { get; set; }
        public string AppName { get; set; }
        public string AppDescription { get; set; }
        public string AppVersion { get; set; }
    }
}
