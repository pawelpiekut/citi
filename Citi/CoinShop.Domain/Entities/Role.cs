﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Entities
{
    public class Role
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<UserRole> UserRole { get; set; }
    }
}
