﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Shared
{
    public static class Constants
    {
        public static readonly int AUTH_TOKEN_EXPIRATION_IN_DAYS = 1;
        public static readonly int ACTIVATION_CODE_EXPIRATION_IN_MINS = 600;
    }
}
