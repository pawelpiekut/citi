﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CoinShop.Domain.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // Marks an entity as new
        Task Add(TEntity entity);

        // Marks an entity as modified
        void Update(TEntity entity);

        // Marks an entity to be removed
        void Delete(TEntity entity);

        void Delete(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false);

        // Get an entity by int id
        Task<TEntity> GetById(int id);

        // Get an entity using delegate
        Task<TEntity> Get(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false);

        Task<bool> Any(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false);

        // Gets all entities of type T
        IQueryable<TEntity> GetAll(bool ignoreQueryFilters = false);

        // Gets entities using delegate
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where, bool ignoreQueryFilters = false);

        Task AddMany(IEnumerable<TEntity> entities);
    }
}
