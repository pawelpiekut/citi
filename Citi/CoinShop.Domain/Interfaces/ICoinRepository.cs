﻿using CoinShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoinShop.Domain.Interfaces
{
    public interface ICoinRepository : IRepository<Coin>
    {
    }
}
