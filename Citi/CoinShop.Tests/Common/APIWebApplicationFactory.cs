﻿using CoinShop.Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WebAPI;

namespace CoinShop.Tests.Common
{
    public class APIWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureTestServices(services =>
            {
                Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "IntegrationTests");
                string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                // Build config
                IConfiguration config = new ConfigurationBuilder()
                    .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{environment}.json", optional: true)
                    .AddEnvironmentVariables()
                .Build();

                var connectionString = config.GetConnectionString("DefaultConnectionLsMssqlDb");

                // remove the existing context configuration
                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ShopContext>));
                if (descriptor != null)
                    services.Remove(descriptor);

                var serviceProvider = new ServiceCollection().BuildServiceProvider();

                // Add a database context (AppDbContext) using an in-memory database for testing.
                services.AddEntityFrameworkSqlServer().AddDbContext<ShopContext>(options =>
                {
                    options.UseSqlServer(connectionString);
                });

                // Build the service provider.
                var sp = services.BuildServiceProvider();

                // Create a scope to obtain a reference to the database contexts
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var lsDb = scopedServices.GetRequiredService<ShopContext>();

                    var logger = scopedServices.GetRequiredService<ILogger<WebApplicationFactory<Startup>>>();
                    lsDb.Database.EnsureDeleted();
                    lsDb.Database.Migrate();
                }
            });
        }
    }
}
