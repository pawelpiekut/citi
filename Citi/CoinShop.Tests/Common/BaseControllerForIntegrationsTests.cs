﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace CoinShop.Tests.Common
{
    [TestFixture]
    public class BaseControllerForIntegrationsTests
    {
        public APIWebApplicationFactory _factory;
        public HttpClient _client;

        [OneTimeSetUp]
        public void SetUp()
        {

            _factory = new APIWebApplicationFactory();
            _client = _factory.CreateClient();
        }
    }
}
