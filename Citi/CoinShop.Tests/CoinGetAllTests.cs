﻿using CoinShop.API.Dtos;
using CoinShop.API.Dtos.Coin;
using CoinShop.Tests.Common;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CoinShop.Tests
{
    [TestFixture]
    public class CoinGetAllTests: BaseControllerForIntegrationsTests
    {
        [Test]
        public async Task TryToGetAllCoinsWithutAuthentication_ExpectedException401()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/Coins/GetAll");
            var content = await response.Content.ReadAsStringAsync();
            var statusCode = (int)response.StatusCode;

            Assert.IsTrue(statusCode == 401);
        }

        [Test]
        public async Task TryToGetAllCoinsWithAuthentication_Expected200()
        {
            var requestLogin = new LoginRequest()
            {
                Username = "pawelp",
                Password = "@dmin1234"
            };

            var json = JsonConvert.SerializeObject(requestLogin);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage responseLogin = await _client.PostAsync("/api/Auth/Login", data);
            string loginResponse = responseLogin.Content.ReadAsStringAsync().Result;
            dynamic respResponseDynamic = JsonConvert.DeserializeObject(loginResponse);
            string token = respResponseDynamic.token;

            HttpResponseMessage responseMessage = null;
            GetAllCoinResponse responseContent = null;
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, "/api/Coins/GetAll"))
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                responseMessage = await _client.SendAsync(requestMessage);
                var contentGetAllCoins = await responseMessage.Content.ReadAsStringAsync();
                responseContent = JsonConvert.DeserializeObject<GetAllCoinResponse>(contentGetAllCoins);
            }

            Assert.NotNull(responseMessage);
            Assert.NotNull(responseContent);

            var statusCode = (int)responseMessage.StatusCode;
            Assert.IsTrue(statusCode == 200);
            Assert.AreEqual(1, responseContent.Coins.Count);
        }
    }
}
